;;; lintel-parser.el --- Parses an elisp file with comments and positions  -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'generator)

(iter-defun lintel-parser-parse-buffer ()
  "Return an iterator yielding parse objects starting at point."
  )

(provide 'lintel-parser)
;;; lintel-parser.el ends here
