;;; lintel-parser-tests.el --- Tests for lintel-parser.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:
(require 'lintel-parser)
(require 'ert)

(defun lintel-parser--should-end (iterator)
  "Assert that ITERATOR is at the end."
  (let ((catched nil))
    (condition-case _
        (iter-next iterator)
      (iter-end-of-sequence
       (setq catched t)))
    (should catched)))

(ert-deftest lintel-parser-return-top-level-entries-at-beginning-of-line ()
  (with-temp-buffer
    (insert "(a)\n(b)\n")
    (setf (point) (point-min))
    (let ((iter (lintel-parser-parse-buffer)))
      (should (equal (iter-next iter) '(a)))
      (should (equal (iter-next iter) 'a))
      (lintel-parser--should-end iter))))


(provide 'lintel-parser-tests)
;;; lintel-parser-tests.el ends here
