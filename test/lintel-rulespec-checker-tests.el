;;; lintel-rulespec-checker-tests.el --- Tests for lintel-rulespec-checker.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'lintel-rulespec-checker)

(ert-deftest lintel-rulespec-checker-check-rulespec-bad-test-when-no-expected-problem ()
  (cl-letf (((symbol-function #'lintel-rulespec-display-name) #'identity)
            ((symbol-function #'lintel--run) #'identity))
    (let* ((test '(:bad "the code"))
           (result (lintel-rulespec-checker-check-rulespec-bad-test 'rulespec test)))
      (should (string=
               result
               "\"the code\" is defined as being a :bad example for rulespec but does not specify any expected problem")))))

(ert-deftest lintel-rulespec-checker-check-rulespec-bad-test-when-empty-expected-problems ()
  (cl-letf (((symbol-function #'lintel-rulespec-display-name) #'identity)
            ((symbol-function #'lintel--run) #'identity))
    (let* ((test '(:bad "the code" :problems ()))
           (result (lintel-rulespec-checker-check-rulespec-bad-test 'rulespec test)))
      (should (string=
               result
               "\"the code\" is defined as being a :bad example for rulespec but does not specify any expected problem")))))

(ert-deftest lintel-rulespec-checker-check-problems-when-all-match ()
  (cl-letf (((symbol-function #'lintel-rulespec-checker-find-matching) (lambda (_1 _2 _3 _4) nil)))
    (let ((result (lintel-rulespec-checker-check-problems '(a b c) '(d e f) 'test 'rulespec)))
      (should (null result)))))

(ert-deftest lintel-rulespec-checker-check-problems-when-any-dont-match ()
  (cl-letf (((symbol-function #'lintel-rulespec-checker-find-matching)
             (lambda (expected-problem _2 _3 _4) (when expected-problem
                                              "error message"))))
    (let ((result (lintel-rulespec-checker-check-problems '(t nil t) '(d e f) 'test 'rulespec)))
      (should (string= result "error message")))))

(ert-deftest lintel-rulespec-checker-find-matching-when-match ()
  (cl-letf (((symbol-function #'lintel-rulespec-checker-match-p) (lambda (_1 actual-problem) actual-problem)))
    (let* ((actual-problems (vector nil nil t nil))
           (result (lintel-rulespec-checker-find-matching 'expected-problem actual-problems 'test 'rulespec)))
      (should (null result))
      (should (equal actual-problems (vector nil nil nil nil))))))

(ert-deftest lintel-rulespec-checker-find-matching-when-no-match ()
  (cl-letf (((symbol-function #'lintel-rulespec-display-name) #'identity)
            ((symbol-function #'lintel-rulespec-checker-match-p) (lambda (_1 _2) nil)))
    (let* ((actual-problems (vector 'a 'b 'c 'd))
           (result (lintel-rulespec-checker-find-matching 'expected-problem actual-problems 'test 'rulespec)))
      (should (string=
               result
               "test was defined as being a :bad example for rulespec but problem expected-problem didn't match any actual problem detected"))
      (should (equal actual-problems (vector 'a 'b 'c 'd))))))

(ert-deftest lintel-rulespec-checker-match-p ()
  (let ((expected-problem-1 '(:start 1 :message "foo"))
        (expected-problem-2 '(:start 2 :message "bar"))
        (actual-problem-1 (lintel--result-create :start 1 :message "foo"))
        (actual-problem-2 (lintel--result-create :start 2 :message "bar")))
    (should (lintel-rulespec-checker-match-p expected-problem-1 actual-problem-1))
    (should (lintel-rulespec-checker-match-p expected-problem-2 actual-problem-2))
    (should-not (lintel-rulespec-checker-match-p expected-problem-1 actual-problem-2))
    (should-not (lintel-rulespec-checker-match-p expected-problem-2 actual-problem-1))
    (should-not (lintel-rulespec-checker-match-p expected-problem-1 nil))
    (should-not (lintel-rulespec-checker-match-p nil actual-problem-1))))

(ert-deftest lintel-rulespec-checker-test-display-string ()
  (let* ((expected "(kbd \"C-c n\")")
         (test '(
                 :bad (kbd "C-c n")
                 :problems '((
                              :start 1
                              :message "This key sequence is reserved"))))
         (actual (lintel-rulespec-checker-test-display-string test)))
    (should (string= actual expected))))

(provide 'lintel-rulespec-checker-tests)
;;; lintel-rulespec-checker-tests.el ends here
