;;; lintel-tests.el --- Tests for lintel.el          -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'lintel)
(require 'ert)

(ert-deftest lintel--list-parent-rules-return-empty-when-no-parent-scope ()
  (let* ((rulesset (lintel--ruleset-create :rules '(rule1) :scope (lintel--scope-create)))
         (result (lintel--list-parent-rules rulesset)))
    (should (hash-table-empty-p result))))

(ert-deftest lintel--list-parent-rules-return-rules-of-parent-scope ()
  (cl-letf (((symbol-function 'lintel--ruleset)
             (lambda (scope)
               (should (eq scope 'parent-scope))
               'parent-ruleset))
            ((symbol-function 'lintel--list-rules)
             (lambda (ruleset)
               (should (eq ruleset 'parent-ruleset))
               'hashtable)))
    (let* ((parent-scope 'parent-scope)
           (rulesset (lintel--ruleset-create
                      :rules '(rule1)
                      :scope (lintel--scope-create :parent parent-scope)))
           (result (lintel--list-parent-rules rulesset)))
      (should (eq result 'hashtable)))))

(ert-deftest lintel--list-rules-with-no-parent-scope ()
  (cl-letf (((symbol-function 'lintel-rule-name) #'identity)
            ((symbol-function 'lintel--list-parent-rules) (lambda (_) (make-hash-table))))
    (let* ((ruleset (lintel--ruleset-create :rules '(rule1 rule2)))
           (result (lintel--list-rules ruleset)))
      (should (equal (map-length result) 2))
      (should (equal (map-elt result 'rule1) 'rule1))
      (should (equal (map-elt result 'rule2) 'rule2)))))

(ert-deftest lintel--list-rules-return-rules-from-parent ()
  (cl-letf (((symbol-function 'lintel-rule-name) #'identity)
            ((symbol-function 'lintel--list-parent-rules)
             (lambda (_) (let* ((rules (make-hash-table)))
                      (setf (map-elt rules 'rule1) 'rule1)
                      (setf (map-elt rules 'rule2) 'rule2)
                      rules))))
    (let* ((ruleset (lintel--ruleset-create))
           (result (lintel--list-rules ruleset)))
      (should (equal (map-length result) 2))
      (should (equal (map-elt result 'rule1) 'rule1))
      (should (equal (map-elt result 'rule2) 'rule2)))))

(ert-deftest lintel--list-rules-overrides-rules-of-parent ()
  (let ((rule1 (lintel--rule-create :rulespec 'rulespec1))
        (rule21 (lintel--rule-create :rulespec 'rulespec2))
        (rule22 (lintel--rule-create :rulespec 'rulespec2)))
    (cl-letf (((symbol-function 'lintel-rule-name) #'lintel-rule-rulespec)
              ((symbol-function 'lintel--list-parent-rules)
               (lambda (_) (let* ((rules (make-hash-table)))
                        (setf (map-elt rules 'rulespec1) rule1)
                        (setf (map-elt rules 'rulespec2) rule21)
                        rules))))
      (let* ((ruleset (lintel--ruleset-create :rules (list rule22)))
             (result (lintel--list-rules ruleset)))
        (should (equal (map-length result) 2))
        (should (eq (map-elt result 'rulespec1) rule1))
        (should (eq (map-elt result 'rulespec2) rule22))))))

(ert-deftest lintel--run-context-with-file-scope-sets-environment ()
  (with-temp-buffer
    (insert "foo\n")
    (narrow-to-region 2 3)
    (let* ((expected-buffer (current-buffer))
           (have-executed-script nil)
           (script (lambda (_context)
                     (should (eq expected-buffer (current-buffer)))
                     (should (equal (point) 1))
                     (should (looking-at-p "foo\n"))
                     (setq have-executed-script t)))
           (rule (lintel--rule-create
                  :rulespec (lintel--rulespec-create
                             :script script
                             :scope-type 'lintel--file-scope)))
           (context (lintel--context-create
                     :rule rule
                     :scope (lintel--file-scope-create :filename "temp-file.el"))))
      (cl-letf (((symbol-function #'find-file-noselect)
                 (lambda (file)
                   (should (string= file "temp-file.el"))
                   expected-buffer)))
        (with-temp-buffer
          (lintel--run context))
        (should have-executed-script)))))

(ert-deftest lintel--run-rule ()
  (let* ((have-executed-script nil)
         (script (lambda (_context) (setq have-executed-script t)))
         (rulespec (lintel--rulespec-create
                    :script script
                    :scope-type 'lintel-file-scope))
         (rule (lintel--rule-create :rulespec rulespec))
         (scope (lintel--file-scope-create :filename "temp-file.el"))
         (ruleset (lintel--ruleset-create :rules (list rule) :scope scope)))
    (lintel--run ruleset)
    (should have-executed-script)))

(ert-deftest lintel--run-skip-rule-with-different-scope ()
  (let* ((script (lambda (_context) (should-not t)))
         (rulespec (lintel--rulespec-create
                    :script script
                    :scope-type 'lintel-directory-scope))
         (rule (lintel--rule-create :rulespec rulespec))
         (scope (lintel--file-scope-create :filename "temp-file.el"))
         (ruleset (lintel--ruleset-create :rules (list rule) :scope scope)))
    (lintel--run ruleset)))

(ert-deftest lintel--sexp-scope-iterator ()
  (let* ((scope (lintel--sexp-scope-create :sexp '(if a (progn b c) d e)))
         (iterator (lintel--sexp-scope-iterator scope)))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'if))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'a))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) '(progn b c)))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'progn))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'b))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'c))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'd))
    (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'e))
    (let ((catched nil))
      (condition-case _
          (iter-next iterator)
        (iter-end-of-sequence
         (setq catched t)))
      (should catched))))

(ert-deftest lintel--buffer-scope-iterator-with-1-top-level ()
  (with-temp-buffer
    (insert "(if (a b))")
    (setf (point) (point-min))
    (let* ((scope (lintel--buffer-scope-create :buffer (current-buffer)))
           (iterator (lintel--buffer-scope-iterator scope)))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) '(if (a b))))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'if))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) '(a b)))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'a))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'b)))))

(ert-deftest lintel--buffer-scope-iterator-with-2-top-level ()
  (with-temp-buffer
    (insert "(a) (b)")
    (setf (point) (point-min))
    (let* ((scope (lintel--buffer-scope-create :buffer (current-buffer)))
           (iterator (lintel--buffer-scope-iterator scope)))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) '(a)))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'a))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) '(b)))
      (should (equal (lintel-sexp-scope-sexp (iter-next iterator)) 'b)))))

(provide 'lintel-tests)
;;; lintel-tests.el ends here
