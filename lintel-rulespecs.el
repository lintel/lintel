;;; lintel-rulespecs.el --- Define a few example rulespecs  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>
;; Url: https://gitlab.com/lintel/lintel
;; Package-requires: ((emacs "26.3"))
;; Version: 0.1.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file defines various rulespecs to show how this can be done.
;; This file will go away before the initial release to clearly split
;; the linting engine from rulespecs.

;;; Code:
(require 'package-lint)
(require 'lintel)

(defun lintel-rulespecs--no-reserved-keybinding (context)
  "Check in CONTEXT that there is no reserved keybinding."
  (let* ((scope (lintel-context-scope context))
         (sexp (lintel-sexp-scope-sexp scope)))
    (when (seq-contains-p '(kbd global-set-key local-set-key define-key)
                          (car sexp))
      (when-let* ((keybinding (package-lint--extract-key-sequence sexp))
                  (message (package-lint--test-keyseq keybinding)))
        (lintel-report
         :context context
         :message message
         :point (point))))))

(lintel-define-rulespec
 :name 'no-reserved-keybinding
 :type 'lintel-sexp-scope
 :script #'lintel-rulespecs--no-reserved-keybinding
 :tests `((
           :bad ,(list 'kbd "C-c n")
           :problems ((
                       :start 1
                       :message "This key sequence is reserved (see Key Binding Conventions in the Emacs Lisp manual)")))
          (
           :good ,(list 'kbd "C-d n"))))

(provide 'lintel-rulespecs)
;;; lintel-rulespecs.el ends here
