;;; lintel-rulespec-checker.el --- Tool checking quality of lintel's rulespecs  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2023  Damien Cassou

;; Author: Damien Cassou <damien@cassou.me>
;; Url: https://gitlab.com/lintel/lintel
;; Package-requires: ((emacs "26.3"))
;; Version: 0.1.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file implements an engine that checks the validity of
;; rulespecs' syntax and good/bad examples.

;;; Code:

(require 'lintel)

(defun lintel-rulespec-checker-buffer ()
  "Return a buffer where to write errors with rulespecs."
  (get-buffer-create "*lintel-rulespec-checker*"))

(defun lintel-rulespec-checker-batch-check ()
  "Check all rulespecs in `lintel-rulespecs'.
Throw an error if there was any error."
  (interactive)
  (message "Checking rulespecs: %s" (mapconcat #'lintel-rulespec-display-name
                                               lintel-rulespecs
                                               " "))
  (lintel-rulespec-checker-check)
  (with-current-buffer (lintel-rulespec-checker-buffer)
    (when (> (point-max) (point-min))
      (message "%s" (buffer-substring-no-properties (point-min) (point-max)))
      (user-error "Some rules where broken"))))

(defun lintel-rulespec-checker-check ()
  "Check all rulespecs in `lintel-rulespecs'.
Write errors in `lintel-rulespec-checker-buffer'."
  (interactive)
  (let ((errors (lintel-rulespec-checker-check-rulespecs lintel-rulespecs))
        (buffer (lintel-rulespec-checker-buffer)))
    (with-current-buffer buffer
      (erase-buffer)
      (dolist (error-message errors)
        (insert error-message "\n----------\n"))
      (switch-to-buffer buffer))))

(defun lintel-rulespec-checker-check-rulespecs (rulespecs)
  "Check all RULESPECS.
Return a list of error messages, one message per failed test."
  (mapcan #'lintel-rulespec-checker-check-rulespec rulespecs))

(defun lintel-rulespec-checker-check-rulespec (rulespec)
  "Check RULESPEC.
Return a list of error messages, one message per failed test."
  (let ((errors (list)))
    (dolist (test (lintel-rulespec-tests rulespec) errors)
      (when-let* ((error-message (lintel-rulespec-checker-check-rulespec-test rulespec test)))
        (setq errors (cons error-message errors))))))

(defun lintel-rulespec-checker-check-rulespec-test (rulespec test)
  "Check that TEST passes for RULESPEC."
  (if (eq (car test) :good)
      (lintel-rulespec-checker-check-rulespec-good-test rulespec test)
    (lintel-rulespec-checker-check-rulespec-bad-test rulespec test)))

(defun lintel-rulespec-checker-check-rulespec-good-test (rulespec test)
  "Assert that TEST triggers no problem with RULESPEC.
Return a string describing how RULESPEC's script failed accepting
TEST.  Return nil if RULESPEC's script returned no problem for TEST."
  (let* ((rule (lintel--rule-create :rulespec rulespec))
         (resultset (lintel--resultset-create :results (list)))
         (sexp (map-elt test :good))
         (scope (lintel--sexp-scope-create :sexp sexp))
         (context (lintel--context-create
                   :rule rule
                   :scope scope
                   :resultset resultset)))
    (lintel--run context)
    (when-let* ((results (lintel-resultset-results resultset)))
      (format "%s was supposed to accept the text below but failed with:\n\n%s\n\nText:\n--\n%s"
              (lintel-rulespec-display-name rulespec)
              (mapconcat (lambda (result) (format "- %s" (lintel-result-message result)))
                         results
                         "\n")
              (format "%S" sexp)))))

(defun lintel-rulespec-checker-check-rulespec-bad-test (rulespec test)
  "Assert that TEST triggers problems with RULESPEC.
Return a string describing how RULESPEC's script failed to detect
a problem in TEST.  Return nil if RULESPEC's script found the
expected problems in TEST."
  (let* ((rule (lintel--rule-create :rulespec rulespec))
         (resultset (lintel--resultset-create :results (list)))
         (sexp (map-elt test :bad))
         (scope (lintel--sexp-scope-create :sexp sexp))
         (context (lintel--context-create
                   :rule rule
                   :scope scope
                   :resultset resultset))
         (expected-problems (map-elt test :problems nil))
         (actual-problems (progn
                            (lintel--run context)
                            (lintel-resultset-results resultset))))
    (cond
     ((not expected-problems)
      (format "%s is defined as being a :bad example for %s but does not specify any expected problem"
              (lintel-rulespec-checker-test-display-string test)
              (lintel-rulespec-display-name rulespec)))
     ((not actual-problems)
      (format "%s is defined as being a :bad example for %s but the rulespec failed to detect any problem"
              test
              (lintel-rulespec-display-name rulespec)))
     (t (lintel-rulespec-checker-check-problems expected-problems actual-problems test rulespec)))))

(cl-defun lintel-rulespec-checker-check-problems (expected-problems actual-problems test rulespec)
  "Check that EXPECTED-PROBLEMS match ACTUAL-PROBLEMS.

TEST is the test of RULESPEC being checked."
  (let ((actual-problems (seq-concatenate 'vector actual-problems)))
    (dolist (expected-problem expected-problems)
      (let ((error-message (lintel-rulespec-checker-find-matching expected-problem actual-problems test rulespec)))
        (when error-message
          (cl-return-from lintel-rulespec-checker-check-problems error-message))))))

(cl-defun lintel-rulespec-checker-find-matching (expected-problem actual-problems test rulespec)
  "Search for an item of ACTUAL-PROBLEMS matching EXPECTED-PROBLEM.
If none is found, return a string containing an error message.
If one is found, return nil.

ACTUAL-PROBLEMS is a vector.  If an item of ACTUAL-PROBLEMS
matches EXPECTED-PROBLEM, the corresponding cell in the array is
set to nil to prevent the matching item from being matched again.

TEST is the test of RULESPEC being checked."
  (dotimes (actual-problem-i (seq-length actual-problems))
    (when (lintel-rulespec-checker-match-p expected-problem (seq-elt actual-problems actual-problem-i))
      ;; an actual problem has matched, prevent it from being
      ;; matched again:
      (setf (seq-elt actual-problems actual-problem-i) nil)
      (cl-return-from lintel-rulespec-checker-find-matching nil)))
  (format "%s was defined as being a :bad example for %s but problem %s didn't match any actual problem detected"
          test
          (lintel-rulespec-display-name rulespec)
          expected-problem))

(defun lintel-rulespec-checker-match-p (expected-problem actual-problem)
  "Return t iff EXPECTED-PROBLEM match ACTUAL-PROBLEM."
  (and
   expected-problem
   actual-problem
   (string= (map-elt expected-problem :message "")
            (lintel-result-message actual-problem))
   (eql (map-elt expected-problem :start)
        (lintel-result-start actual-problem))))

(defun lintel-rulespec-checker-test-display-string (test)
  "Return a string describing TEST for error messages."
  (format "%S" (nth 1 test)))

(provide 'lintel-rulespec-checker)
;;; lintel-rulespec-checker.el ends here
