ELPA_DEPENDENCIES=package-lint

ELPA_ARCHIVES=melpa-stable gnu

TEST_ERT_FILES		= $(wildcard test/*.el)
LINT_CHECKDOC_FILES	= $(wildcard *.el) ${TEST_ERT_FILES}
LINT_PACKAGE_LINT_FILES	= $(wildcard *.el)
LINT_COMPILE_FILES	= ${LINT_CHECKDOC_FILES}

makel.mk:
	# Download makel
	@if [ -f ../makel/makel.mk ]; then \
		ln -s ../makel/makel.mk .; \
	else \
		curl \
		--fail --silent --show-error --insecure --location \
		--retry 9 --retry-delay 9 \
		-O https://github.com/DamienCassou/makel/raw/v0.6.0/makel.mk; \
	fi

# Include makel.mk if present
-include makel.mk

.PHONY: test-rulespecs

test: test-rulespecs

test-rulespecs:
	@echo "# Check rulespecs…"; \
	output=$$(${BATCH} \
	--load lintel-rulespecs \
	--load lintel-rulespec-checker \
	--funcall lintel-rulespec-checker-batch-check 2>&1) \
	|| ( echo "$${output}" && exit 1 );
